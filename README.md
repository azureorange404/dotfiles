# dotfiles

## about

This is my collection of config files, which (excluding awesomewm) I use on my daily basis.
These configs eventually might not work for you without modifing them yourself.

## included

### program configurations

- awesomewm (outdated)
- bash
- dmscripts (by [distrotube](https://gitlab.com/dwt1))
- fish
- kitty
- lf
- rofi (outdated)
- slock (now running betterlockscreen)
- vim
- xmonad
- xmobar
- [picom](https://github.com/pijulius/picom)

### shell scripts

- battery notifier
```
To run this one you need to start the systemd service timer:

systemctl --user enable batnotify.timer
systemctl --user start batnotify.timer
```

- autolocker
- lf launcher incl. ueberzug

### assets

- systemsounds

## ownership

The base for my battery notifier script is from [arindas](https://github.com/arindas/batnotify).

The systemsounds were grabbed from websites which offer free downloads. If any of these are yours and you do not want them to be shared on here, contact me and I will remove them.
