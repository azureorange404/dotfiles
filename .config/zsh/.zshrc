# zsh config
HISTFILE="${XDG_STATE_HOME:-$HOME/.local/state}"/zsh/histfile
HISTSIZE=1000000
SAVEHIST=1000000

autoload -U colors && colors
setopt beep extendedglob nomatch autocd
unsetopt autocd notify
stty stop undef

[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/env" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/env"

# prompt
source <(/usr/bin/starship init zsh --print-full-init)

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' cache-path $XDG_CACHE_HOME/zsh/zcompcache
zstyle ':completion:*' matcher-list '' \
  'm:{a-z\-}={A-Z\_}' \
  'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
  'r:|?=** m:{a-z\-}={A-Z\_}'
zmodload zsh/complist
compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION
_comp_options+=(globdots)

bindkey -v
# export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';;      # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    # zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp -uq)"
    trap 'rm -f $tmp >/dev/null 2>&1 && trap - HUP INT QUIT TERM PWR EXIT' HUP INT QUIT TERM PWR EXIT
    /usr/bin/lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
bindkey -s '^o' '^ulfcd\n'
bindkey -s '^[^f' '^ucd "$(find -L . -type d -print | fzf)"\n'
bindkey -s '^f' '^uopen "$(find -L . -print | fzf)"\n'

bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

autoload -U select-word-style
select-word-style bash
bindkey "^w" backward-kill-word
bindkey "^u" backward-kill-line

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line
bindkey -M vicmd '^[[P' vi-delete-char
bindkey -M vicmd '^e' edit-command-line
bindkey -M visual '^[[P' vi-delete

# Load completion hinting.
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
# Load syntax highlighting; should be last.
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null

default_wm="/home/azure/.config/zsh/default-wm"

startwm() {
    echo "$1" >"$default_wm"

    # Xmonad
    if [ "$1" = 1 ]; then
        /usr/bin/cp $HOME/.config/fish/xinitrc-xmonad $XINITRC
        exec startx
    # DWM
    elif [ "$1" = 2 ]; then
        /usr/bin/cp $HOME/.config/fish/xinitrc-dwm $XINITRC
        exec startx
    # NEWM
    elif [ "$1" = 3 ]; then
        exec newm-run
    # Hyprland
    elif [ "$1" = 4 ]; then
        export WLR_NO_HARDWARE_CURSORS=1
        export WLR_RENDERER_ALLOW_SOFTWARE=1
        exec Hyprland
    # Default
    else
        /usr/bin/cp $HOME/.config/fish/xinitrc-dwm $XINITRC
        exec startx
    fi
}

if [[ -o login ]]; then
    if [[ -z "$DISPLAY" ]] && [[ "$XDG_VTNR" = 1 ]]; then
        echo "Hello"
        _window_managers=("xmonad" "dwm" "newm" "hyprland")
        j=1
        
        echo "Choose the window manager to run:"
        
        for i in $_window_managers
        do
            echo "($j) $i"
            j=$(( j + 1 ))
        done
       
        read -t 5 _choice

        if [ "$_choice" = "" ]; then
            _choice="$(cat $default_wm)"
            [ "$_choice" = "" ] && startwm 2
        fi

        startwm "$_choice" || startwm 2
        
    fi
else
    # printf "\n%s\n\n" "$(shell-quote | sed -e 's/;\ /;\n/g' -e 's/\.\ /\.\n/g' -e 's/,\ /,\n/g')"
    printf "\n -  %s\n\n" "$(hyprctl splash)"
    # echo -en "\x1b[2J\x1b[1;1H" ; echo; echo; seq 1 (tput cols) | sort -R | lolcat; echo; echo
fi
