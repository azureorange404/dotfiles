#!/usr/bin/env python3
from __future__ import annotations
from typing import Callable, Any

import os, random
# import filetype
import mimetypes
import logging
import subprocess
import sys
from pathlib import Path

from newm.layout import Layout
from newm.helper import BacklightManager, WobRunner, PaCtl

from pywm import (
    PYWM_MOD_LOGO,
    PYWM_MOD_ALT
)

def on_startup():
    os.system("systemctl --user import-environment DISPLAY WAYLAND_DISPLAY XDG_CURRENT_DESKTOP")
    os.system("hash dbus-update-activation-environment 2>/dev/null && \
        dbus-update-activation-environment --systemd DISPLAY \
        WAYLAND_DISPLAY XDG_CURRENT_DESKTOP")
    os.system("dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=wlroots")
    # os.system("/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1") # GNOME authentication agent to run root applications
    os.system("dunst -conf $HOME/.config/dunst/dunstrc &")
    os.system("gnome-keyring-daemon --start --components=secrets &")
    os.system("dbus-update-activation-environment --all &")
    os.system("waybar &")
    os.system("flameshot &")
    os.system("nm-applet --indicator &")
    os.system("blueman-applet &")
    os.system("udiskie --no-automount --smart-tray --file-manager='kitty -e lfub' &")
    # os.system("birdtray &")
    # os.system("numlockx on &")
    os.system("sleep 5 && nextcloud --background &")
    os.system("syncthing serve --no-browser &")
    os.system("kitty --title='timeshift' -o background_opacity=1 --hold sudo timeshift --check &")

def on_reconfigure():
    os.system("killall waybar &")
    os.system("waybar &")
    gnome_schema = 'org.gnome.desktop.interface'
    wm_service_extra_config = (
        f"export QT_QPA_PLATFORMTHEME='qt5ct'",
        f"gsettings set {gnome_schema} gtk-theme 'Catppuccin-Mocha-B'",  # change to the theme of your choice
        f"gsettings set {gnome_schema} icon-theme 'Catppuccin_Dark'",  # change to the icon of your choice
        f"gsettings set {gnome_schema} cursor-theme 'Catppuccin-Mocha-Light-Cursors'",  # change to the cursor of your choice
        f"gsettings set {gnome_schema} font-name 'Lucida MAC 10'",  # change to the font of your choice
        f"gsettings set org.gnome.desktop.interface color-scheme prefer-dark",
        f"gsettings set org.blueman.plugins.powermanager auto-power-on false",
    )
    os.system("notify-send newm \"Reloaded config\" &")

    for config in wm_service_extra_config:
        config = f"{config} &"
        os.system(config)

logger = logging.getLogger(__name__)

# Random file picker (required libraries: os, sys, random, Path (pathlib))

def get_random_files(top=os.getcwd()):
    file_list = list(Path(top).glob(f"**/*.*"))
    if not len(file_list):
        os.environ['NEWM_BACKGROUND_ERROR'] = background_path
        os.system("notify-send \"newm\" \"no images found in: $NEWM_BACKGROUND_ERROR\"")
    rand = random.randint(0, len(file_list) - 1)
    return file_list[rand]

background_path = os.environ['HOME'] + '/.wallpapers/'

background = {
    # takes set value (example configuration)
    # 'path': os.environ['HOME'] + '/.wallpapers/wallpapers/0056.jpg', # example config

    # chooses a random numbered jpg (documentation)
    # "path": os.environ["HOME"] + f"/images/random/bg-{randrange(1, 5)}.jpg",

    # chooses a random file, but results in a black background if a directory was chosen
    # 'path': background_path + random.choice(os.listdir(background_path)),

    # chooses a random file, checks if it actually is a file, but results in a black background if non-image was chosen
    # 'path': background_path + random.choice([x for x in os.listdir(background_path) if os.path.isfile(os.path.join(background_path, x))]),

    # check if it is an actual image (requires random, mimetypes)
    # 'path': background_path + random.choice([x for x in os.listdir(background_path) if "image" in str(mimetypes.guess_type(os.path.join(background_path, x)))]),

    # pick a random file from the chosen top directory
    # 'path': get_random_files1("jpg", background_path),
    'path': get_random_files(background_path),
    'anim': True
}

outputs = [
    { 'name': 'eDP-1' },
    { 'name': 'virt-1', 'pos_x': -1280, 'pos_y': 0, 'width': 1280, 'height': 720 }
]

wob_runner = WobRunner("wob -a bottom -M 100")
backlight_manager = BacklightManager(anim_time=1., bar_display=wob_runner)
kbdlight_manager = BacklightManager(args="--device='*::kbd_backlight'", anim_time=1., bar_display=wob_runner)
def synchronous_update() -> None:
    backlight_manager.update()
    kbdlight_manager.update()

pactl = PaCtl(0, wob_runner)

pywm = {
    'xkb_model': "acer_laptop",
    'xkb_layout': "ch",
    'xkb_options': "caps:escape",
    'xkb_numlock': "enabled",
    'enable_xwayland': True,
    'encourage_csd': False,
    'pywm.contstrain_popups_to_toplevel': True,
    'xcursor_theme': "Catppuccin-Mocha-Light-Cursors",
    'xcursor_size': 36,
    'focus_follows_mouse': False, # is problematic with floating windows that close without focus
}

def rules(view):
    common_rules = {
        "opacity": 1,
        "float": True,
        "float_size": (450, 250),
        "float_pos": (0.5, 0.5),
    }
    big_rules = {
        "opacity": 1,
        "float": True,
        "float_size": (1450, 830),
        "float_pos": (0.5, 0.5),
    }
    float_app_ids = (
        "pavucontrol",
        "blueman-manager",
        "nm-connection-editor",
        "Pinentry-gtk-2",
    )
    big_float_app_ids = (
        "com.nextcloud.desktopclient.nextcloud",
        "yad",
    )
    float_titles = ("Dialect",)
    blur_apps = ("rofi")
    app_rule = None
    # os.system(
    #     f"echo '{view.app_id}, {view.title}, {view.role}, {view.up_state.is_floating}' >> ~/.config/newm/apps"
    # )
    # os.system(
    #         f"export CURRENT_WAYLAND_WINDOW=$(echo '{view.title}')"
    # )
    os.environ['CURRENT_WAYLAND_WINDOW'] = view.title # visible in this process + all children
    # Set float common rules
    if view.app_id == "catapult":
        app_rule = {"float": True, "float_pos": (0.5, 0.1)}
    # elif view.title is not None and "compartir indicador" in view.title.lower():
    #     return {"float": True, "float_size": (30, 20)}
    elif view.title is not None and "timeshift" in view.title.lower():
        return {"float": True, "float_pos": (0.25, 0.35), "float_size": (750, 500)}
    elif view.title is not None and "floating" in view.title.lower():
        return {"float": True, "float_pos": (0.25, 0.35), "float_size": (750, 500)}
    elif view.up_state.is_floating:
        app_rule = common_rules
    elif view.app_id in float_app_ids or view.title in float_titles:
        app_rule = common_rules
    elif view.app_id in big_float_app_ids or view.title in big_float_titles:
        app_rule = big_rules
    elif view.app_id in blur_apps:
        app_rule = {"blur": {"radius": 5, "passes": 6}}
    if view.app_id == "dmenu":
        app_rule = {"float": True, "float_pos": (0.5, 0.1)}
    return app_rule

view = {
    'ssd': {
        'color': '#ffffffff',
        'enabled': False
    },
    'rules': rules,
}

# def synchronous_update(view):
#         NOTIF = "notify-send 'Fuck'" + "this" + "' &"
#         os.system(NOTIF)
#         os.environ['CURRENT_WAYLAND_WINDOW'] = title

corner_radius = 0

def export_view(view):
    os.environ['CURRENT_WAYLAND_WINDOW']: title # visible in this process + all children


def key_bindings(layout: Layout) -> list[tuple[str, Callable[[], Any]]]:
    return [

    # WINDOW CONTROL
        # ("L-h", lambda: layout.move(-1, 0)),
        ("L-Left", lambda: [
            layout.move(-1, 0),
            # os.system("kitty &")
            # os.environ['CURRENT_WAYLAND_WINDOW']: view.title # visible in this process + all children
            export_view(view)
            ]),
        # ("L-j", lambda: layout.move(0, 1)),
        ("L-Down", lambda: layout.move(0, 1)),
        # ("L-k", lambda: layout.move(0, -1)),
        ("L-Up", lambda: layout.move(0, -1)),
        # ("L-l", lambda: layout.move(1, 0)),
        ("L-Right", lambda: layout.move(1, 0)),
        # ("L-u", lambda: layout.basic_scale(1)),
        ("L-A-Down", lambda: layout.basic_scale(1)),
        # ("L-n", lambda: layout.basic_scale(-1)),
        ("L-A-Up", lambda: layout.basic_scale(-1)),
        # ("L-t", lambda: layout.move_in_stack(1)),

        # ("L-H", lambda: layout.move_focused_view(-1, 0)),
        ("L-S-Left", lambda: layout.move_focused_view(-1, 0)),
        # ("L-J", lambda: layout.move_focused_view(0, 1)),
        ("L-S-Down", lambda: layout.move_focused_view(0, 1)),
        # ("L-K", lambda: layout.move_focused_view(0, -1)),
        ("L-S-Up", lambda: layout.move_focused_view(0, -1)),
        # ("L-L", lambda: layout.move_focused_view(1, 0)),
        ("L-S-Right", lambda: layout.move_focused_view(1, 0)),

        # ("L-C-h", lambda: layout.resize_focused_view(-1, 0)),
        ("L-C-Left", lambda: layout.resize_focused_view(-1, 0)),
        # ("L-C-j", lambda: layout.resize_focused_view(0, 1)),
        ("L-C-Down", lambda: layout.resize_focused_view(0, 1)),
        # ("L-C-k", lambda: layout.resize_focused_view(0, -1)),
        ("L-C-Up", lambda: layout.resize_focused_view(0, -1)),
        # ("L-C-l", lambda: layout.resize_focused_view(1, 0)),
        ("L-C-Right", lambda: layout.resize_focused_view(1, 0)),

    # NEWM
        ("L-S-r", lambda: os.system("newm-cmd update-config &")),
        # ("L-S-r", lambda: [
        #     os.system("newm-cmd update-config &"), 
        #     os.system("kitty &")
        #     ]),
    
    # HELP
        ("L-F1", lambda: os.system("bash ~/.config/shell/scripts/help/newm_keys.sh &")),
        ("L-F2", lambda: os.system("bash ~/.config/shell/scripts/help/vim_keys.sh &")),
        ("L-F3", lambda: os.system("bash ~/.config/shell/scripts/help/lf_keys.sh &")),
        ("L-F4", lambda: os.system("bash ~/.config/shell/scripts/help/curriculum.sh &")),

    # WOFI
        ("L-S-Return", lambda: os.system("wofi --gtk-dark --insensitive --show run &")),
        ("L-S-l", lambda: os.system("wofi-logout &")),
        ("L-p p", lambda: os.system("passmenu-otp &")),
        ("L-p m", lambda: os.system("wofi-mail &")),
        ("L-p t", lambda: os.system("wofi-todo &")),
        ("L-p n", lambda: os.system("wofi-note &")),
        ("L-p c", lambda: os.system("wofi-cal &")),
        ("L-p k", lambda: os.system("wofi-kill &")),
        ("L-p h", lambda: os.system("wofi-hub &")),
        
    # PROGRAMS
        ("L-Return", lambda: os.system("kitty &")),

        # browsers
        ("L-b b", lambda: os.system("qutebrowser &")),
        ("L-b f", lambda: os.system("firefox &")),
        ("L-b o", lambda: os.system("brave --enable-features=UseOzonePlatform --ozone-platform=wayland &")),
        
        # messaging
        ("L-m m", lambda: os.system("thunderbird &")),
        ("L-m t", lambda: os.system("teams &")),
        ("L-m w", lambda: os.system("whatsapp-for-linux &")),
        ("L-m q", lambda: os.system("signal-desktop &")),
        ("L-m s", lambda: os.system("slack &")),

        # file managers
        ("L-e e", lambda: os.system("kitty -e lfub &")),
        ("L-e q", lambda: os.system("pcmanfm &")),
        ("L-e p", lambda: os.system("pcmanfm &")),

        ("L-L", lambda: os.system("dm-logout &")),
        ("L-q", lambda: layout.close_focused_view()),

        # ("L-p", lambda: layout.ensure_locked(dim=True)),
        # ("L-P", lambda: layout.terminate()),
        ("L-C", lambda: layout.update_config()),

        ("L-f", lambda: [
            layout.toggle_fullscreen(),
            os.system("killall -SIGUSR1 waybar")
            ]),

        # ("L-", lambda: layout.toggle_overview()),
        ("L-space", lambda: layout.toggle_overview()),

        # ("L-t", lambda: os.environ['CURRENT_WAYLAND_WINDOW'] = "fuck you"),
        # ("L-z", lambda: os.environ['CURRENT_WAYLAND_WINDOW'] = 'fuck me'),

        ("XF86MonBrightnessUp", lambda: backlight_manager.set(backlight_manager.get() + 0.1)),
        ("XF86MonBrightnessDown", lambda: backlight_manager.set(backlight_manager.get() - 0.1)),
        ("XF86KbdBrightnessUp", lambda: kbdlight_manager.set(kbdlight_manager.get() + 0.1)),
        ("XF86KbdBrightnessDown", lambda: kbdlight_manager.set(kbdlight_manager.get() - 0.1)),
        ("XF86AudioRaiseVolume", lambda: pactl.volume_adj(5)),
        ("XF86AudioLowerVolume", lambda: pactl.volume_adj(-5)),
        ("XF86AudioMute", lambda: pactl.mute()),

        ("A-XF86MonBrightnessUp", lambda: backlight_manager.set(backlight_manager.get() + 0.01)),
        ("A-XF86MonBrightnessDown", lambda: backlight_manager.set(backlight_manager.get() - 0.01)),
        ("A-XF86KbdBrightnessUp", lambda: kbdlight_manager.set(kbdlight_manager.get() + 0.01)),
        ("A-XF86KbdBrightnessDown", lambda: kbdlight_manager.set(kbdlight_manager.get() - 0.01)),

        ("L-v", layout.toggle_focused_view_floating),
        # ("L-v", layout.move_next_view),
        ("L-t", lambda: layout.move_in_stack(4)),
        # ("L-w", lambda: background.path('$HOME/.wallpapers/anime/jujutsukaizen_02.jpg')),
    ]

panels = {
    'lock': {
        'cmd': 'kitty -o background_opacity=1 -e newm-panel-basic lock',
        'corner_radius': 0,
        'h': 1.0,
        'w': 1.0,
    },
    'launcher': {
        'cmd': 'kitty -e newm-panel-basic launcher'
    },
    'top_bar': {
        'native': {
            'enabled': True,
            'texts': "",
            'height': 36,
            # 'texts': lambda: [
            #     # pwd.getpwuid(os.getuid())[0],
            #     # time.strftime("%c"),
            #     # pwd.getpwuid(os.getuid())[0],
            #     f'cpu: {psutil.cpu_percent(interval=1)}%\
            #         mem: {psutil.virtual_memory().percent}%\
            #         ssd: {psutil.disk_usage("/").percent}%\
            #         hdd: {psutil.disk_usage("/mnt/hdd").percent}%\
            #     {"↑" if psutil.sensors_battery().power_plugged else "↓"}{psutil.sensors_battery().percent}%',
            #     time.strftime("%c")
            # ],
        }
    },
    # 'bottom_bar': {
    #     'native': {
    #         'enabled': True,
    #         'texts': lambda: [
    #             "newm",
    #             "powered by pywm"
    #         ],
    #     }
    # },
}

energy = {
    "idle_times": [300, 600, 1500],
    'idle_callback': backlight_manager.callback
}

grid = {
    'throw_ps': [2, 10]
}

