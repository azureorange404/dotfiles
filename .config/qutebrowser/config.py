import catppuccin

# load your autoconfig, use this if the rest of your config is empty!
config.load_autoconfig()

config.set ("content.headers.user_agent", 'Linux / Firefox 82')

# set editor
config.set("editor.command", ["kitty", "-e", "nvim", "{}"])

# Automatically save user session
config.set("auto_save.session", True)

# Change Downloads tab position
config.set("downloads.position","bottom")

# enable dark mode
# config.set("colors.webpage.darkmode.enabled", True)

# font
config.set("fonts.default_size", "9pt")

# adblocking
# config.set("content.blocking.enabled", "true", ["https://gitlab.com"])
# config.set("content.blocking.adblock.lists", ["https://raw.githubusercontent.com/brave/adblock-lists/master/brave-unbreak.txt"])
# config.set("content.blocking.method", "auto")
# config.set("content.blocking.whitelist", ["*://gitlab.com/*"])

# disable autoplay
config.set("content.autoplay", False)

# set the flavour you'd like to use
# valid options are 'mocha', 'macchiato', 'frappe', and 'latte'
catppuccin.setup(c, 'mocha')

# set home page
c.url.start_pages = ["https://cloud.graspingspriggan.ch/login"]

# set new tab page
# config.set("url.default_page", "https://searx.fmac.xyz/")
config.set("url.default_page", "https://wiby.me/")

# Input mode settings
config.set("input.mode_override", "passthrough", "cloud.graspingspriggan.ch")

# set default search engine
# c.url.searchengines["DEFAULT"] = "https://searx.fmac.xyz/search?q={}"
c.url.searchengines["DEFAULT"] = "https://search.brave.com/search?q={}"

# KeyBindings
config.unbind('<Shift-Escape>', mode='passthrough')
config.bind('<Alt-Escape>', 'mode-leave', mode='passthrough')
config.bind('<Ctrl-h>', 'spawn --userscript nextcloud', mode='passthrough')
config.bind('<Ctrl-Shift-h>', 'back', mode='passthrough')
config.bind('<Ctrl-Shift-l>', 'forward', mode='passthrough')
config.bind('<F5>', 'reload', mode='passthrough')
config.bind('<Ctrl-l>', 'spawn --userscript auto-pass -U secret -u "username: (.+)" --dmenu-invocation dmenu', mode='passthrough')
config.bind('<Ctrl-l>', 'spawn --userscript auto-pass -U secret -u "username: (.+)" --dmenu-invocation dmenu', mode='normal')

# toggle dark mode
# Allow joining commands with ;;
def bind_chained(key, *commands):
    config.bind(key, ' ;; '.join(commands))

# Bind 'td' to toggle darkmode
config.bind('td', 'config-cycle colors.webpage.darkmode.enabled ;; restart'    )

# Aliases
c.aliases["mpv"] = "spawn --userscript ~/.config/qutebrowser/userscripts/view_in_mpv"
