# Set programs that you use
$terminal = st

$menu = dmenu_path | rofi -show run

$browser = qutebrowser
$workBrowser = brave

$killSelf = pkill Hyperland

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER


# Special workspace "kill"
bind = $mainMod, Return, exec, $terminal

bind = $mainMod, Q, exec, sleep .4 && hyprctl dispatch submap reset
bind = $mainMod, Q, submap, kill
submap = kill

bind = , Q, killactive
bind = , Q, submap, reset

bind = , Escape, submap, reset
submap = reset

bind = $mainMod SHIFT, Q, exec, sleep .4 && hyprctl dispatch submap reset
bind = $mainMod SHIFT, Q, submap, hyprkill
submap = hyprkill

bind = , Q, exec, pkill Hyprland
bind = , Q, submap, reset

bind = , Escape, submap, reset
submap = reset


# Window control
bind = $mainMod, V, togglefloating,
bind = $mainMod, R, exec, $menu
bind = $mainMod SHIFT, Return, exec, $menu
# bind = $mainMod, P, pseudo, # dwindle
bind = $mainMod, T, togglesplit, # dwindle

# bind = $mainMod, M, exec, [workspace 6 silent] thunderbird

# Toggle waybar
bind = $mainMod, Escape, exec, pkill -SIGUSR1 '^waybar$'

# Update Wallpaper
bind = $mainMod, W, exec, autowp

# Dmenu / rofi
bind = $mainMod, P, exec, sleep 5 && hyprctl dispatch submap reset
bind = $mainMod, P, submap, dmenu
submap = dmenu
bind = , H, exec, rofi-hub
bind = , H, submap, reset
bind = , P, exec, rofi-pass
bind = , P, submap, reset
bind = , M, exec, rofi-mail
bind = , M, submap, reset
bind = , L, exec, rofi-logout
bind = , L, submap, reset
bind = , K, exec, rofi-kill
bind = , K, submap, reset
bind = , Escape, submap, reset
submap = reset

bind= $mainMod, Space, exec, wl-windows

# Move focus with mainMod + arrow keys
bind = $mainMod, K, cyclenext, prev
bind = $mainMod, J, cyclenext

bind = $mainMod, up, cyclenext, prev
bind = $mainMod, down, cyclenext

# Master window control
bind = $mainMod, backspace, layoutmsg, swapwithmaster 

# Example special workspace (scratchpad)
bind = $mainMod, S, togglespecialworkspace, magic
bind = $mainMod SHIFT, S, movetoworkspace, special:magic

bind = $mainMod, M, togglespecialworkspace, messaging
bind = $mainMod SHIFT, M, movetoworkspace, special:messaging

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, L, workspace, e+1
bind = $mainMod, H, workspace, e-1

bind = $mainMod, right, workspace, e+1
bind = $mainMod, left, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

# MultiMediaKeys
binde=, XF86AudioRaiseVolume, exec, wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%+
binde=, XF86AudioLowerVolume, exec, wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 1%-
binde=, XF86MonBrightnessUp, exec, brightnessctl s 1%+
binde=, XF86MonBrightnessDown, exec, brightnessctl s 1%-

# OBS bindings
bind = CONTROL SHIFT,HOME,pass,^(com\.obsproject\.Studio)$
bind = CONTROL SHIFT,PAGE_UP,pass,^(com\.obsproject\.Studio)$
bind = CONTROL ALT,HOME,pass,^(com\.obsproject\.Studio)$
bind = CONTROL SHIFT,PRINT,pass,^(com\.obsproject\.Studio)$

# Source additional config files
# source = ~/.config/hypr/modules/binds/screenshot.conf
# bind = $mainMod, PRINT, exec, sh ~/.config/hypr/scripts/snapshot.sh
bind = $mainMod      , PRINT, exec, pgrep "slurp" || grim -g "0,0 $(hyprctl monitors | head -n2 | tail -n1 | cut -d@ -f1 | sed 's/\s//g')" - | swappy -f -
bind = $mainMod SHIFT, PRINT, exec, pgrep "slurp" || grim -g "$(slurp)" - | swappy -f -
source = ~/.config/hypr/modules/binds/filemanager.conf
source = ~/.config/hypr/modules/binds/workspaces.conf

bind = $mainMod SHIFT, R, exec, hyprctl reload; killall waybar; waybar &
# bindl = ,switch:on:Lid Switch,exec,swaylock -f -c 000000 -i /home/azure/.config/hypr/wallpaper.jpg --daemonize && systemctl suspend
# bindl = ,switch:off:Lid Switch,exec,swaylock

gestures {
    workspace_swipe = true
    # workspace_swipe_invert = true
    # workspace_swipe_min_speed_to_force=5
}
