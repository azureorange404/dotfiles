#!/usr/bin/env bash

pgrep "slurp" || grim -q 100 -g "$(slurp)" - | swappy -f -
