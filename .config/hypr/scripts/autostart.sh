#!/bin/bash

numlockx &
( swww-daemon || swww init --no-daemon ) &
# swww img /home/azure/.config/hypr/wallpaper.jpg &
( sleep 5 && autowp ) &

pgrep waybar || waybar &
pgrep nm-applet || nm-applet &
pgrep nextcloud || ( sleep 15 && nextcloud --background & )
pgrep udiskie || udiskie -a -t &

echo "enabled" >/home/azure/.local/state/lockstate &
# pgrep swayidle || swayidle timeout 180 'lock-wrapper' &

# anacron -t /home/azure/.local/share/cron/etc/anacrontab -S /home/azure/.local/share/cron/var/spool/anacron &
# exec swayidle -w \
#     timeout 60 'anacron -t /home/azure/.local/share/cron/etc/anacrontab -S /home/azure/.local/share/cron/var/spool/anacron &' \
#     timeout 300 '/home/azure/.local/bin/lock'
