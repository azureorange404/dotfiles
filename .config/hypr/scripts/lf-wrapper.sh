#!/usr/bin/env bash
# runs st with lf if it is not already running on special:filemanager

# custom_conf="/tmp/lfrc.custom"

if ! hyprctl -j clients | \
    jq -r '.[] | select(.monitor != -1 ) | "\(.workspace.name)\t\t\(.title)    \(.address)"' | \
    grep "special:filemanager" | grep "lf" 2>/dev/null
then
    # awk '1;/^cmd open/{ 
    # print "    if hyprctl activewindow | grep \"workspace: -\" >/dev/null 2>&1; then";
    # print "        hyprctl dispatch togglespecialworkspace filemanager";
    # print "        hyprctl dispatch submap reset";
    # print "    fi";
    # }' /home/azure/.config/lf/lfrc > "$custom_conf"
    # echo "map q # HYPRLAND" >> "$custom_conf"
    # st -e lf -config $custom_conf
    st -e lf
fi

    # print "    if hyprctl activewindow | grep \"workspace: -95\" >/dev/null 2>&1; then";
    # print "        hyprctl dispatch togglespecialworkspace filemanager";
    # print "        hyprctl dispatch submap reset";
    # print "    fi";
