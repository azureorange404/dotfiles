#!/bin/sh

notify-send "hyprland" "special:filemanager ready"

handle() {
  case $1 in
    'destroyworkspace'*'special:filemanager') hyprctl dispatch submap reset ;;
    "openwindow>>"[0-9]*",special:filemanager,Pcmanfm"*) return ;;
    "openwindow>>"[0-9]*",special:filemanager,St,lf") return ;;
    "openwindow>>"[0-9]*",special:filemanager,"*)
        active_workspace="$(hyprctl activeworkspace | head -n1 | cut -d' ' -f3)"
        hyprctl dispatch movetoworkspace "$active_workspace" && \
        hyprctl dispatch submap reset
    ;;
  esac
}

socat -U - UNIX-CONNECT:$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock | \
    while read -r line;
    do
        handle "$line";
    done
