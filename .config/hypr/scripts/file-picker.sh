#!/bin/sh
#

if ! [ "$1" = "-" ]; then
    exit 1
fi

tmp="$(mktemp -uq)"
trap 'rm -f $tmp >/dev/null 2>&1 && trap - HUP INT QUIT TERM PWR EXIT' HUP INT QUIT TERM PWR EXIT
st -e /usr/bin/lf -last-dir-path="$tmp" "$@" 2>/dev/null
if [ -f "$tmp" ]; then
    dir="$(cat "$tmp")"
    [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && echo "$dir"
fi

