<div align = center>

<img src="https://raw.githubusercontent.com/vaxerski/Hyprland/main/assets/header.svg" width="750" height="300" alt="banner">

<br>

Hyprland is a dynamic tiling Wayland compositor based on wlroots that doesn't sacrifice on its looks.

It provides the latest Wayland features, is highly customizable, has all the eyecandy, the most powerful plugins,
easy IPC, much more QoL stuff than other wlr-based compositors and more...
<br>

</div>

# Configuration

I've split my hyprland configuration into several different files.

There are some things I'd like to mention as I consider them quite special:

## Filemanager Workspace

I created a special workspace 'special:filemanager' which is used for holding my filemanager only.

This configurations consists of three seperate files:

- [./modules/binds/filemanager.conf](modules/binds/filemanager.conf)
- [./scripts/lf-wrapper.sh](scripts/lf-wrapper.sh)
- [./scripts/lf-workspace.sh](scripts/lf-workspace.sh)

The config file creates the special workspace and alongside it a submap for keybindings:
All keybindings (with a few exeptions) are disabled on special:filemanager to ensure its functionality.

The wrapper script makes shure only one instance of $filemaanger is openend when switching to special:filemanager but not when disengaging the workspace.

The workspace script uses hyprlands socket2 to manage windows spawned from special:workspace.
- Every generated window not being 'pcmanfm' or 'st -e lf' will be moved to the active workspace.
- The submap for special:filemanager will be reset, if the workspace is destroyed.
