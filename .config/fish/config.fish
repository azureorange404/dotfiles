### ADDING TO THE PATH
# First line removes the path; second line sets it.  Without the first line,
# your path gets massive and fish becomes very slow.
set -e fish_user_paths
#set -U fish_user_paths $HOME/.local/bin $HOME/Applications $fish_user_paths
set -U fish_user_paths /home/azure/.local/applications/ /home/azure/perl5/bin /home/azure/.local/bin /home/azure/Applications /usr/local/bin /usr/bin /bin /usr/local/games /usr/games /home/azure/.local/bin /home/azure/.vim/bundle/vim-live-latex-preview/bin /var/lib/flatpak/exports/share /home/azure/.local/share/flatpak/exports/share /home/azure/.android/sdk $fish_user_paths

### EXPORT ###
set fish_greeting                 # Supresses fish's intro message
set TERM "xterm-256color"         # Sets the terminal type
set EDITOR "nvim"                  # $EDITOR use Emacs in terminal
set VISUAL "nvim"                  # $VISUAL use Emacs in GUI mode
set JAVA_HOME "/usr/lib/jvm/java-19-openjdk"
set ANDROID_HOME "~/.android/sdk"
if test -e ~/.config/env
	source ~/.config/env
else
	echo "No file env found ..."
end

### SET MANPAGER
### Uncomment only one of these!

### "bat" as manpager
#set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

### "vim" as manpager
set -x MANPAGER '/bin/bash -c "vim -MRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" -c \"nmap q :qa<CR>\"</dev/tty <(col -b)"'

### "nvim" as manpager
# set -x MANPAGER "nvim -c 'set ft=man' -"

### SET EITHER DEFAULT EMACS MODE OR VI MODE ###
function fish_user_key_bindings
  # fish_default_key_bindings
  fish_vi_key_bindings
end
### END OF VI MODE ###

### AUTOCOMPLETE AND HIGHLIGHT COLORS ###
set fish_color_normal brcyan
set fish_color_autosuggestion '#7d7d7d'
set fish_color_command brcyan
set fish_color_error '#ff6c6b'
set fish_color_param brcyan

### SPARK ###
set -g spark_version 1.0.0

complete -xc spark -n __fish_use_subcommand -a --help -d "Show usage help"
complete -xc spark -n __fish_use_subcommand -a --version -d "$spark_version"
complete -xc spark -n __fish_use_subcommand -a --min -d "Minimum range value"
complete -xc spark -n __fish_use_subcommand -a --max -d "Maximum range value"

function spark -d "sparkline generator"
    if isatty
        switch "$argv"
            case {,-}-v{ersion,}
                echo "spark version $spark_version"
            case {,-}-h{elp,}
                echo "usage: spark [--min=<n> --max=<n>] <numbers...>  Draw sparklines"
                echo "examples:"
                echo "       spark 1 2 3 4"
                echo "       seq 100 | sort -R | spark"
                echo "       awk \\\$0=length spark.fish | spark"
            case \*
                echo $argv | spark $argv
        end
        return
    end

    command awk -v FS="[[:space:],]*" -v argv="$argv" '
        BEGIN {
            min = match(argv, /--min=[0-9]+/) ? substr(argv, RSTART + 6, RLENGTH - 6) + 0 : ""
            max = match(argv, /--max=[0-9]+/) ? substr(argv, RSTART + 6, RLENGTH - 6) + 0 : ""
        }
        {
            for (i = j = 1; i <= NF; i++) {
                if ($i ~ /^--/) continue
                if ($i !~ /^-?[0-9]/) data[count + j++] = ""
                else {
                    v = data[count + j++] = int($i)
                    if (max == "" && min == "") max = min = v
                    if (max < v) max = v
                    if (min > v ) min = v
                }
            }
            count += j - 1
        }
        END {
            n = split(min == max && max ? "▅ ▅" : "▁ ▂ ▃ ▄ ▅ ▆ ▇ █", blocks, " ")
            scale = (scale = int(256 * (max - min) / (n - 1))) ? scale : 1
            for (i = 1; i <= count; i++)
                out = out (data[i] == "" ? " " : blocks[idx = int(256 * (data[i] - min) / scale) + 1])
            print out
        }
    '
end
### END OF SPARK ###


### FUNCTIONS ###
# Spark functions
function letters
    cat $argv | awk -vFS='' '{for(i=1;i<=NF;i++){ if($i~/[a-zA-Z]/) { w[tolower($i)]++} } }END{for(i in w) print i,w[i]}' | sort | cut -c 3- | spark | lolcat
    printf  '%s\n' 'abcdefghijklmnopqrstuvwxyz'  ' ' | lolcat
end

function commits
    git log --author="$argv" --format=format:%ad --date=short | uniq -c | awk '{print $1}' | spark | lolcat
end

# Functions needed for !! and !$
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end
# The bindings for !! and !$
if [ $fish_key_bindings = "fish_vi_key_bindings" ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

# Function for creating a backup file
# ex: backup file.txt
# result: copies file as file.txt.bak
function backup --argument filename
    cp $filename $filename.bak
end

# Function for copying files and directories, even recursively.
# ex: copy DIRNAME LOCATIONS
# result: copies the directory and all of its contents.
function copy
    set count (count $argv | tr -d \n)
    if test "$count" = 2; and test -d "$argv[1]"
	set from (echo $argv[1] | trim-right /)
	set to (echo $argv[2])
        command cp -r $from $to
    else
        command cp $argv
    end
end

# Function for printing a column (splits input on whitespace)
# ex: echo 1 2 3 | coln 3
# output: 3
function coln
    while read -l input
        echo $input | awk '{print $'$argv[1]'}'
    end
end

# Function for printing a row
# ex: seq 3 | rown 3
# output: 3
function rown --argument index
    sed -n "$index p"
end

# Function for ignoring the first 'n' lines
# ex: seq 10 | skip 5
# results: prints everything but the first 5 lines
function skip --argument n
    tail +(math 1 + $n)
end

# Function for taking the first 'n' lines
# ex: seq 10 | take 5
# results: prints only the first 5 lines
function take --argument number
    head -$number
end

# Function for org-agenda
function org-search -d "send a search string to org-mode"
    set -l output (/usr/bin/emacsclient -a "" -e "(message \"%s\" (mapconcat #'substring-no-properties \
        (mapcar #'org-link-display-format \
        (org-ql-query \
        :select #'org-get-heading \
        :from  (org-agenda-files) \
        :where (org-ql--query-string-to-sexp \"$argv\"))) \
        \"
    \"))")
    printf $output
end

# LaTeX

function create_new_tex_project --argument projectname
	echo 'Generating ./$projectname/$projectname.tex and its source ...'
    cp -r ~/.latex/master ./$projectname
    cp ~/.latex/template.tex ./$projectname/$projectname.tex
    vim ./$projectname/$projectname.tex
end

function create_new_tex_document --argument documentname
	echo 'Generating ./$documentname.tex ...'
	cp ~/.latex/template.tex ./$documentname.tex
	vim ./$documentname.tex
end

function create_new_tex_article_project --argument projectname
	echo 'Generating ./$projectname/$projectname.tex and its source ...'
	cp -r ~/.latex/master ./$projectname
	cp ~/.latex/article.tex ./$projectname/$projectname.tex
	vim ./$projectname/$projectname.tex
end

function create_new_tex_article --argument documentname
	echo 'Generating ./$documentname.tex ...'
	cp ~/.latex/article.tex ./$documentname.tex
	vim ./$documentname.tex
end

function create_new_tex_worksheet_project --argument projectname
	echo 'Generating ./$projectname/$projectname.tex and its source'
	cp -r ~/.latex/master ./$projectname
	cp ~/.latex/worksheet.tex ./$projectname/$projectname.tex
	vim ./$projectname/$projectname.tex
end

function create_new_tex_worksheet --argument documentname
	echo 'Generating ./$documentname.tex ...'
	cp ~/.latex/worksheet.tex ./$documentname.tex
	vim ./$documentname.tex
end

function create_new_tex_presentation --argument documentname
	echo 'Generating ./$documentname.tex ...'
	cp ~/.latex/presentation.tex ./$documentname.tex
	vim ./$documentname.tex
end

### END OF FUNCTIONS ###


### ALIASES ###
# \x1b[2J   <- clears tty
# \x1b[1;1H <- goes to (1, 1) (start)
alias clear='echo -en "\x1b[2J\x1b[1;1H" ; echo; echo; seq 1 (tput cols) | sort -R | spark | lolcat; echo; echo'

function update_webpage; 
		while true;
				echo Keep running
				echo "Press CTRL+C to exit"
				sudo /usr/lib/cgi-bin/test.cgi > /var/www/html/posting/count.html
				sleep 1
		end;
end

### Aliases

if test -e ~/.config/shell/aliasrc;
	source ~/.config/shell/aliasrc
else
	echo "No file aliasrc found ..."
end

# Get this script from my GitLab: gitlab.com/dwt1/shell-color-scripts
# Or install it from the Arch User Repository: shell-color-scripts
#colorscript random

### Theme

if test -e ~/.config/fish/theme.fish;
    source ~/.config/fish/theme.fish
end

# Start X at login

if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        # exec startx -- -keeptty
        set _window_managers "xmonad" "dwm" "newm"
        set j 1
        
        echo "Choose the window manager to run:"
        
        for i in $_window_managers
            echo "($j) $i"
            set j ( math $j + 1 )
        end
       
        read -t 5 _choice
        
        if [ "$_window_managers[$_choice]" = "xmonad" ] 
            /usr/bin/cp $HOME/.config/fish/xinitrc-xmonad $XINITRC
            exec startx
        else if [ "$_window_managers[$_choice]" = "dwm" ]
            /usr/bin/cp $HOME/.config/fish/xinitrc-dwm $XINITRC
            exec startx
        else if [ "$_window_managers[$_choice]" = "newm" ]
            exec newm-run
        else
            /usr/bin/cp $HOME/.config/fish/xinitrc-dwm $XINITRC
            exec startx
        end
    end
end

# setxkbmap -model acer_laptop -layout ch

echo -en "\x1b[2J\x1b[1;1H" ; echo; echo; seq 1 (tput cols) | sort -R |     spark | lolcat; echo; echo
