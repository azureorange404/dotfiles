#!/bin/fish

# HEADER

set fish_greeting
echo; seq 1 (tput cols) | sort -R | spark | lolcat; echo

# LF ICONS

if test -e ~/.config/lf/icons.conf;
    source ~/.config/lf/icons.conf
end

# STARSHIP PROMPT

export STARSHIP_CONFIG=/home/azure/.config/fish/starship.toml

starship init fish | source
