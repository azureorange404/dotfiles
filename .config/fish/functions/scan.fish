function scan
  set scanners (scanimage -L | awk '{print NR-1 ") " $0}')
  printf '%s\n' $scanners
  
  echo "Enter the number of the scanner you choose."
  read number

  set device (printf '%s\n' $scanners | grep "$number) " | cut -d ' ' -f 3 | awk '{print substr($0, 2, length($0) - 2)}')
  echo "You chose $device"

  echo "Where do you want to save the scanned document? (path/filename.ext)"
  read fullfile

  set filename (path basename $fullfile)
  set extension (path extension $fullfile | sed -e 's/.//')
  
  echo "scanning ..."

  scanimage --progress --device "$device" --format=$extension --output-file $fullfile

  echo "Document $filename was saved in $fullfile"
end
