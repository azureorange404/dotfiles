-- Treesitter settings
require'nvim-treesitter.configs'.setup {
    ensure_installed = "all", -- one of "all", "maintained" (parsers with maintainers)
    highlight = {
        enable = true -- false will disable the whole extension
    }
}

