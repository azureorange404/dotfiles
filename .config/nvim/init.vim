set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.config/vim/vimrc

highlight Normal           guifg=#faffcb ctermfg=15  guibg=#20202033 ctermbg=none cterm=none
" highlight Normal           guifg=#dfdfdf ctermfg=15  guibg=#282c34 ctermbg=none cterm=none
highlight LineNr           guifg=#5b6268 ctermfg=8   guibg=#202020 ctermbg=none cterm=none
" highlight LineNr           guifg=#5b6268 ctermfg=8   guibg=#282c34 ctermbg=none cterm=none
highlight CursorLineNr     guifg=#121214 ctermfg=7   guibg=#5b6268 ctermbg=8    cterm=none
highlight VertSplit        guifg=#121214 ctermfg=0   guibg=#5b6268 ctermbg=8    cterm=none
highlight Statement        guifg=#589979 ctermfg=2   guibg=none    ctermbg=none cterm=none
highlight Directory        guifg=#32bcbc ctermfg=4   guibg=none    ctermbg=none cterm=none
highlight StatusLine       guifg=#202328 ctermfg=7   guibg=#5b6268 ctermbg=8    cterm=none
highlight StatusLineNC     guifg=#202328 ctermfg=7   guibg=#5b6268 ctermbg=8    cterm=none
highlight NERDTreeClosable guifg=#589979 ctermfg=2
highlight NERDTreeOpenable guifg=#5b6268 ctermfg=8
highlight Comment          guifg=#32bcbc ctermfg=4   guibg=none    ctermbg=none cterm=italic
highlight Constant         guifg=#3e63bc ctermfg=12  guibg=none    ctermbg=none cterm=none
highlight Special          guifg=#32bcbc ctermfg=4   guibg=none    ctermbg=none cterm=none
highlight Identifier       guifg=#5699af ctermfg=6   guibg=none    ctermbg=none cterm=none
highlight PreProc          guifg=#bf6b85 ctermfg=5   guibg=none    ctermbg=none cterm=none
highlight String           guifg=#3e63bc ctermfg=12  guibg=none    ctermbg=none cterm=none
highlight Number           guifg=#e7574e ctermfg=1   guibg=none    ctermbg=none cterm=none
highlight Function         guifg=#e7574e ctermfg=1   guibg=none    ctermbg=none cterm=none
highlight Visual           guifg=#faffcb ctermfg=1   guibg=#1c1f24 ctermbg=none cterm=none

 " let g:startify_custom_header =
       " \ startify#pad(split(system('figlet -f Block -w 100 NEOVIM | lolcat'), '\n'))

" Android Development Configuration

set shell=/bin/sh

" Set up plugins
call plug#begin('~/.local/share/nvim/plugged')
" Plug 'vim-scripts/android.vim'
Plug 'hsanson/vim-android'
Plug 'dense-analysis/ale'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neoclide/coc-java', {'branch': 'master'}
Plug 'neoclide/coc-vimtex', {'branch': 'master'}
Plug 'L3MON4D3/LuaSnip', {'tag': 'v2.2.0', 'do': 'make install_jsregexp'}
Plug 'junegunn/fzf.vim'
Plug 'salkin-mada/openscad.nvim'
Plug 'lervag/vimtex'
Plug 'terryma/vim-multiple-cursors'
" Plug 'weirongxu/coc-markdown-preview-enhanced', {'branch': 'main'}
Plug 'majutsushi/tagbar'
Plug 'tpope/vim-fugitive'
"Plug 'nvim-treesitter/nvim-treesitter'
Plug 'nvim-telescope/telescope-file-browser.nvim'

Plug 'iamcco/markdown-preview.nvim'
Plug 'dhruvasagar/vim-table-mode'

Plug 'mg979/vim-visual-multi'
call plug#end()

" Coc Settings
let g:coc_global_extensions = [
  \ 'coc-java',
  \ 'coc-xml'
\ ]
  " \ 'coc-markdown'

" vim-android
" airline
let g:airline_theme='bubblegum'

call airline#parts#define_function(
       \ 'gradle-running',
       \ 'lightline#gradle#running'
       \)
call airline#parts#define_function(
       \ 'gradle-errors',
       \ 'lightline#gradle#errors'
       \)
call airline#parts#define_function(
       \ 'gradle-warnings',
       \ 'lightline#gradle#warnings'
       \)
call airline#parts#define_function(
       \ 'gradle-project',
       \ 'lightline#gradle#project'
       \)

let g:airline_section_x= airline#section#create_right([
     \ 'filetype',
     \ 'gradle-running',
     \ 'gradle-errors',
     \ 'gradle-warnings'
     \])

" ALE
let g:gradle_loclist_show = 0
let g:gradle_show_signs = 0
let g:ale_linters = {
     \ 'xml': ['android'],
     \ 'groovy': ['android'],
     \ 'java': ['android', 'checkstyle', 'eclipselsp'],
     \ 'kotlin': ['android', 'ktlint', 'languageserver']
     \ }

" vim-android
let g:android_sdk_path = '/home/azure/.android/sdk'
let g:gradle_daemon=1
let g:gradle_show_signs=0

if exists('$TMUX')
    autocmd BufEnter * call system("tmux rename-window " . expand("%:t"))
endif
" Treesitter Configuration
"luafile $HOME/.config/nvim/treesitter.lua
"luafile $HOME/.config/nvim/telescope.lua
"luafile $HOME/.config/nvim/remember.lua
"source $HOME/.config/nvim/coc-keybindings.vim
"
"inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<CR>"

" convert office formats
augroup filetype_docx
    autocmd BufReadPost *.docx :%!pandoc -f docx -t markdown
    autocmd BufWrite *.docx :%!pandoc -f markdown -t docx % > hallo.docx
augroup END

luafile $HOME/.config/nvim/openscad.lua

" Compile document, be it groff/LaTeX/markdown/etc.
    map <leader>c :w! \| :!compiler "%:p" \>/tmp/compiler.log && if grep "Emergency stop" /tmp/compiler.log; then notify-send "compiler" "error: view log in /tmp/compiler.log"; fi<CR>
    " map <leader>c :w! \| :silent !compiler "%:p" >/tmp/compiler.log && if (( grep "Error" )); then notify-send "compiler" "error: see log /tmp/compiler.log"; else notify-send "compiler" "success"; fi<CR>
    " map <leader>C :w! \| :silent !wordcount "%:p"<CR>

" Present with sent
    map <leader>p :w! \| !sent "%:p"<CR>
