#!/bin/sh

# map the first button to Super+Left
xsetwacom set "Wacom Intuos BT M Pad pad" Button 1 "key +super left -super"
# map the second button to Super+Right
xsetwacom set "Wacom Intuos BT M Pad pad" Button 2 "key +super right -super"
# map the third button to Ctrl+\+ and \+
xsetwacom set "Wacom Intuos BT M Pad pad" Button 3 "Key +Ctrl +Shift + -Ctrl + -Shift"
# map the fourth button to Ctrl+\- and \-
xsetwacom set "Wacom Intuos BT M Pad pad" Button 8 "Key +Ctrl - -Ctrl -"

# xsetwacom set "Wacom Intuos BT M Pen stylus" Button 3 "Key +f20"

notify-send "Wacom" "Buttons remapped"
