# xmonad config

## about

This is my xmonad config.

My Configuration is split into multiple files (in the way I managed to do best, this will probably improve over time). 

The main config file is [xmonad.hs](https://gitlab.com/azureorange404/dotfiles/-/blob/main/.xmonad/xmonad.hs), the modules I created for better readablility of the otherwise very long config are found in [./lib/ArchOrange/](https://gitlab.com/azureorange404/dotfiles/-/tree/main/.xmonad/lib/ArchOrange).

## xmobar

My xmobar is configured in xmonad.hs alongside with its own config file in [../.config/xmobar/xmobarrc](https://gitlab.com/azureorange404/dotfiles/-/blob/main/.config/xmobar/xmobarrc).

(I tried to strip the xmobar config from xmobar.hs out into it's own module, but I failed heavily.)

## building with stack

Somewhere along my journey I learned that the best way to install xmonad and xmobar was by using stack. So I followed a guide by [Brian Buccola](https://brianbuccola.com/how-to-install-xmonad-and-xmobar-via-stack/).

By going this route, all the problems I experienced before (weren't that many though) were no more, as the source code was directly pulled from their git repositories and bleeding edge as I liked it.

## ownership

I first started using xmonad with the configuration from [DistroTube](https://gitlab.com/dwt1), but I then started editing it and making changes (so don't wonder if there are similarities, especially in comments along the documents).

## self-explanatory

Do not hesitate to read the documentation on both [xmonad](https://xmonad.org/documentation.html) and [xmobar](https://codeberg.org/xmobar/xmobar/src/branch/master/doc) and read the [ArchWiki](https://wiki.archlinux.org/title/Xmonad) as well.
