module Main (main) where

  -- Base
import XMonad
import System.IO (hPutStrLn)
import qualified XMonad.StackSet as W

    -- Data
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events
import XMonad.Hooks.ManageDocks (docksEventHook, manageDocks)

   -- Utilities
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.NamedScratchpad (namedScratchpadFilterOutWorkspacePP)

   -- Color scheme
import Colors.DoomOne

   -- ArchOrange
import ArchOrange.Commands
import ArchOrange.KeyBindings (myKeyBindings)
import ArchOrange.AutoStart (myStartupHook)
import ArchOrange.Layout (myLayoutHook, myWorkspaces, clickable)
import ArchOrange.ManageHook (myManageHook, myHandleEventHook)

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

main = do
    xmproc0 <- spawnPipe ("xmobar -x 0 ~/.config/xmobar/xmobarrc")
    xmonad $ ewmh $ myKeyBindings def
        { manageHook         = myManageHook <+> manageDocks
        , handleEventHook    = docksEventHook <+> myHandleEventHook
        , modMask            = myModMask
        , terminal           = myTerminal
        , startupHook        = myStartupHook
        , layoutHook         = myLayoutHook
        , workspaces         = myWorkspaces
        , borderWidth        = myBorderWidth
        , normalBorderColor  = myNormColor
        , focusedBorderColor = myFocusColor
        , logHook = dynamicLogWithPP $ namedScratchpadFilterOutWorkspacePP $ xmobarPP
              -- XMOBAR SETTINGS
              { ppOutput = \x -> hPutStrLn xmproc0 x   -- xmobar on monitor 1
              , ppCurrent = xmobarColor color06 "" . wrap
                            ("[") "]" -- Current workspace
              , ppVisible = xmobarColor color06 "" . clickable -- Visible but not current workspace
              , ppHidden = xmobarColor color12 "" . wrap
                           -- ("<box type=Top width=2 mt=2 color=" ++ color12 ++ ">") "</box>" . clickable -- Hidden workspace -- with gaps
                           ("<box type=Top width=2 mt=5 color=" ++ color12 ++ ">") "</box>" . clickable -- Hidden workspace -- borderless
              , ppHiddenNoWindows = xmobarColor color12 ""  . clickable -- Hidden workspaces (no windows)
              , ppTitle = xmobarColor color16 "" . shorten 60 -- Title of active window
              , ppSep =  "<fc=" ++ color09 ++ "> <fn=1>|</fn> </fc>" -- Separator character
              , ppUrgent = xmobarColor color02 "" . wrap "!" "!" -- Urgent workspace
              , ppExtras  = [windowCount] -- Adding # of windows on current workspace to the bar
              , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t] -- order of things in xmobar
              }
        }

