module ArchOrange.AutoStart (
  ArchOrange.AutoStart.myStartupHook
) where

   -- Base
import XMonad

   -- Hooks
import XMonad.Hooks.SetWMName

   -- Utilities
import XMonad.Util.SpawnOnce

   -- Color scheme
import Colors.DoomOne

   -- ArchOrange
import ArchOrange.Commands as AO (myStartupSound)

myStartupHook :: X ()
myStartupHook = do

    -- spawnOnce "xmobar ~/.config/xmobar/xmobar.config"
    spawnOnce "xmobar ~/.config/xmobar/xmobarrc"

    -- play startup sound
    spawnOnce AO.myStartupSound
    -- spawn "setxkbmap -model acer_laptop -layout ch"

    spawn "killall trayer"  -- kill current trayer on each restart
    -- spawn ("sleep 2 && trayer --edge top --align right --widthtype request --padding 8 --margin 40 --distance 12 --iconspacing 5 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 60 --tint 0x202020 --height 22") -- with gaps
    spawn ("sleep 2 && trayer --edge top --align right --widthtype request --padding 8 --margin 30 --distance 4 --iconspacing 5 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 60 --tint 0x202020 --height 22") -- borderless

    spawnOnce "bash /home/azure/.config/shell/scripts/lock/locker.sh"
    spawnOnce "xss-lock --ignore-sleep -- /home/azure/.config/shell/scripts/lock/lock -p -c"

    spawnOnce "flameshot"
    -- spawn "killall compton && compton --config ~/.config/picom.conf"
    spawnOnce "picom"
    -- spawnOnce "picom --config ~/.config/picom.conf"

    --TRAYER applets
    spawnOnce "nm-applet --indicator" --netwoekmanager
    spawnOnce "blueman-applet" --bluetooth
    spawnOnce "udiskie --no-automount --smart-tray --file-manager='kitty -e lfub'" --disk mounting
    spawnOnce "birdtray" --thunderbird
    
    -- NOTIFICATIONS
    -- spawnOnce "/usr/lib/notification-daemon/notification-daemon"
    -- spawnOnce "tiramisu"
    spawnOnce "dunst -conf $HOME/.config/dunst/dunstrc"
    -- spawnOnce "aw-qt"
    -- spawnOnce "pnmixer"
    spawnOnce "numlockx on"
    spawnOnce "systemctl --user enable batnotify.timer && systemctl --user start batnotify.timer"

    -- WALLPAPER
    -- spawnOnce "feh --randomize --bg-fill ~/.wallpapers/*"  -- feh set random wallpaper
    spawnOnce "/home/azure/.config/shell/scripts/background/background.sh"  -- feh set random wallpaper

    -- SYNCING
    spawnOnce "dbus-update-activation-environment --all" --gnome-keyring nextcloud
    spawnOnce "gnome-keyring-daemon --start --components=secrets" --again
    spawnOnce "nextcloud"
    spawnOnce "syncthing serve --no-browser"
    -- spawnOnce "a2ln 1997"

    spawnOnce "kitty --title='timeshift' --hold sudo timeshift --check"
    
    setWMName "LG3D"

