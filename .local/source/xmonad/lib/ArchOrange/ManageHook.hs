module ArchOrange.ManageHook (
  ArchOrange.ManageHook.myManageHook,
  ArchOrange.ManageHook.myHandleEventHook
) where

    -- Base
import XMonad
import qualified XMonad.StackSet as W

import XMonad.Actions.CopyWindow ( copyToAll )
    -- Utilities
import XMonad.Util.NamedScratchpad (namedScratchpadManageHook, customFloating)

    -- Data
import Data.Monoid

    -- Hooks
import XMonad.Hooks.WindowSwallowing
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)

    -- ArchOrange
import ArchOrange.Scratchpad (myScratchPads)
import ArchOrange.Layout (myWorkspaces)

manageToDo = customFloating $ W.RationalRect l t w h
                where
                  h = 0.5
                  w = 0.4
                  t = 0.75 -h
                  l = 0.70 -w

manageCal  = customFloating $ W.RationalRect l t w h
                where
                  h = 0.6
                  w = 0.5
                  t = 0.80 -h
                  l = 0.75 -w

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- 'doFloat' forces a window to float, 'doCenterFloat' to float screen centered
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8! (first workspace is 0)
     [ className =? "confirm"                   --> doFloat
     , className =? "file_progress"             --> doFloat
     , className =? "dialog"                    --> doFloat
     , className =? "download"                  --> doFloat
     , className =? "error"                     --> doFloat
     , className =? "notification"              --> doFloat
     , className =? "pinentry-gtk-2"            --> doFloat
     , className =? "splash"                    --> doFloat
     , className =? "toolbar"                   --> doFloat
     , className =? "Yad"                       --> doCenterFloat
     , className =? "Nextcloud"                 --> doFloat
     , title     =? "mpvFloating"               --> doCenterFloat
     , title     =? "Change Foreground Color"   --> doCenterFloat
     , title     =? "Change Background Color"   --> doCenterFloat
     , title     =? "Change color of selected text" --> doCenterFloat
     , title     =? "todo"                      --> manageToDo
     , title     =? "calendar"                  --> manageCal
     -- , title     =? "mpvFloating"               --> doF copyToAll
     , title     =? "udiskie"                   --> doCenterFloat
     , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat
     -- www
     , className =? "Brave-browser"             --> doShift ( myWorkspaces !! 1 )
     , className =? "qutebrowser"               --> doShift ( myWorkspaces !! 1 )
     , className =? "Opera"                     --> doShift ( myWorkspaces !! 1 )
     -- doc
     -- , className =? "Zathura"                   --> doShift ( myWorkspaces !! 3 )
     , className =? "libreoffice"               --> doShift ( myWorkspaces !! 3 )
     , className =? "libreoffice-startcenter"   --> doShift ( myWorkspaces !! 3 )
     , className =? "libreoffice-writer"        --> doShift ( myWorkspaces !! 3 )
     , className =? "libreoffice-calc"          --> doShift ( myWorkspaces !! 3 )
     , className =? "libreoffice-impress"       --> doShift ( myWorkspaces !! 3 )
     , className =? "libreoffice-math"          --> doShift ( myWorkspaces !! 3 )
     , title     =? "LibreOffice"               --> doShift ( myWorkspaces !! 3 )
     -- gam
	 , className =? "Virt-manager"              --> doShift ( myWorkspaces !! 4)
     , (className =? "Virt-manager" <&&> title =? "win7 on QEMU/KVM User session") --> doFullFloat
     -- chat
     , className =? "thunderbird"               --> doShift ( myWorkspaces !! 5 )
     , className =? "teams-for-linux"           --> doShift ( myWorkspaces !! 5 )
     -- med
     , className =? "Firefox-esr"               --> doShift ( myWorkspaces !! 6 )
     , title     =? "Mozilla Firefox"           --> doShift ( myWorkspaces !! 6 )
     , (className =? "mpv" <&&> title =? "video0 - mpv") --> doFloat
     -- gfx
     , className =? "Gimp"                      --> doShift ( myWorkspaces !! 7 )
     , className =? "onshape-nativefier-9f9441" --> doShift ( myWorkspaces !! 7 )
     , className =? "libreoffice-draw"          --> doShift ( myWorkspaces !! 7 )
     -- vlt
     , className =? "KeePassXC"                 --> doShift ( myWorkspaces !! 8 )
     ] <+> (isFullscreen --> doFullFloat) <+> namedScratchpadManageHook myScratchPads

myHandleEventHook = swallowEventHook (className =? "kitty") (return True)

