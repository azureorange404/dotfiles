module ArchOrange.Commands (
  ArchOrange.Commands.myFont,
  ArchOrange.Commands.myModMask,
  ArchOrange.Commands.myTerminal,
  ArchOrange.Commands.myScript,
  ArchOrange.Commands.myFiles,
  ArchOrange.Commands.myCalendar,
  ArchOrange.Commands.myBrowser,
  ArchOrange.Commands.myMail,
  ArchOrange.Commands.myMusic,
  ArchOrange.Commands.myScreenshot,
  ArchOrange.Commands.myRegionScreenshot,
  ArchOrange.Commands.myDelayedScreenshot,
  ArchOrange.Commands.myEditor,
  ArchOrange.Commands.myBorderWidth,
  ArchOrange.Commands.myNormColor,
  ArchOrange.Commands.myFocusColor,
  ArchOrange.Commands.mySound,
  ArchOrange.Commands.myStartupSound,
  ArchOrange.Commands.myKillSound,
  ArchOrange.Commands.myShiftSound,
  ArchOrange.Commands.myKill1,
  ArchOrange.Commands.myShiftTo
) where

   -- Base
import XMonad
import qualified XMonad.StackSet as W

   -- Actions
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, doTo, WSType(..), nextScreen, prevScreen)

   -- Utilities
import XMonad.Util.WorkspaceCompare (getSortByIndex) -- used in myShiftTo

   -- Color scheme
import Colors.DoomOne

myFont :: String
--myFont = "xft:SauceCodePro Nerd Font Mono:regular:size=9:antialias=true:hinting=true"
myFont = "xft:Ubuntu:bold:size=11:antialias=true:hinting=true"

myModMask :: KeyMask
myModMask = mod4Mask        -- Sets modkey to windows key

myTerminal :: String
myTerminal = "kitty "    -- Sets default terminal

myScript :: String
myScript = "/home/azure/.config/shell/scripts/"

myFiles :: String
myFiles = "pcmanfm"
-- myFiles = "kitty -e fish -c lfub"

myCalendar :: String
myCalendar = "flatpak run re.sonny.Tangram"

myBrowser :: String
myBrowser = "qutebrowser"  -- Sets brave as browser

myMail :: String
myMail = "thunderbird "

myMusic :: String
myMusic = "spotify & ~/.config/shell/scripts/xmobar/spotify_title.sh"

myScreenshot :: String
myScreenshot = "flameshot screen -p ~/Pictures/screenshots"

myRegionScreenshot :: String
myRegionScreenshot = "flameshot gui -p ~/Pictures/screenshots"

myDelayedScreenshot :: String
myDelayedScreenshot = "flameshot screen -p ~/Pictures/screenshots -d 5000"

-- myEmacs :: String
-- myEmacs = "emacsclient -c -a 'emacs' "  -- Makes emacs keybindings easier to type

myEditor :: String
myEditor = "neovide "    -- Sets vim as editor
-- myEditor = myTerminal ++ " -e vim "    -- Sets vim as editor

myBorderWidth :: Dimension
myBorderWidth = 2           -- Sets border width for windows

myNormColor :: String       -- Border color of normal windows
myNormColor   = colorBack   -- This variable is imported from Colors.THEME

myFocusColor :: String      -- Border color of focused windows
myFocusColor  = color15     -- This variable is imported from Colors.THEME

-- Actions and Sounds

mySound :: String
mySound = "ffplay -nodisp -volume 50 -autoexit ~/.config/sounds/"

myStartupSound :: String
myStartupSound = mySound ++ "startup.wav"

myKillSound :: String
myKillSound = mySound ++ "azurian_plop.wav"

myShiftSound :: String
myShiftSound = mySound ++ "whoosh.wav"

myKill1 :: X ()
myKill1 = do ss <- gets windowset
             whenJust (W.peek ss) $ \w -> if W.member w $ delete'' w ss
                                        then windows $ delete'' w
                                        else kill <+> spawn myKillSound
    where delete'' w = W.modify Nothing (W.filter (/= w))

myShiftTo :: Direction1D -> WSType -> X ()
myShiftTo dir t = do ss <- gets windowset
                     whenJust (W.peek ss) $ \w -> if W.member w $ delete'' w ss
                                                then windows $ delete'' w
                                                else doTo dir t getSortByIndex (windows . W.shift) <+> spawn myShiftSound <+> doTo dir t getSortByIndex (windows . W.greedyView)
    where delete'' w = W.modify Nothing (W.filter (/= w))

