module ArchOrange.StatusBar (
    ArchOrange.StatusBar.myPP,
    ArchOrange.StatusBar.mySB,
    ArchOrange.StatusBar.myXMobar
) where

  -- Base
import XMonad
import System.IO (hPutStrLn)
import qualified XMonad.StackSet as W

import XMonad.Actions.CopyWindow

    -- Data
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events
import XMonad.Hooks.ManageDocks (docksEventHook, manageDocks)
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import qualified XMonad.Hooks.StatusBar as SB

   -- Utilities
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.NamedScratchpad (namedScratchpadFilterOutWorkspacePP)

   -- Color scheme
import Colors.DoomOne

import ArchOrange.Layout (myLayoutHook, myWorkspaces, clickable)

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myPP :: X PP
myPP =
  -- xmproc0 <- spawnPipe ("xmobar -x 0 ~/.config/xmobar/xmobarrc")
  copiesPP (xmobarColor "#f9e2af" "" . \s -> "<fn=2>\63099</fn>")
   $ def
    -- XMOBAR SETTINGS
    { --ppOutput = \x -> hPutStrLn xmproc0 x   -- xmobar on monitor 1
      ppCurrent = xmobarColor color06 "" . wrap
                  ("[") "]" -- Current workspace
    , ppVisible = xmobarColor color06 "" . clickable -- Visible but not current workspace
    , ppHidden = xmobarColor color12 "" . wrap
                 ("<box type=Top width=2 mt=2 color=" ++ color12 ++ ">") "</box>" . clickable -- Hidden workspace
    , ppHiddenNoWindows = xmobarColor color12 ""  . clickable -- Hidden workspaces (no windows)
    , ppTitle = xmobarColor color16 "" . shorten 60 -- Title of active window
    , ppSep =  "<fc=" ++ color09 ++ "> <fn=1>|</fn> </fc>" -- Separator character
    , ppUrgent = xmobarColor color02 "" . wrap "!" "!" -- Urgent workspace
    , ppExtras  = [windowCount] -- Adding # of windows on current workspace to the bar
    , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t] -- order of things in xmobar
    }

mySB :: FilePath -> SB.StatusBarConfig
mySB xmobarPath = (SB.statusBarProp xmobarPath myPP)
    { SB.sbCleanupHook = SB.killAllStatusBars
    , SB.sbStartupHook = SB.killAllStatusBars >> SB.spawnStatusBar xmobarPath
    }

myXMobar :: StatusBarConfig
myXMobar = statusBarProp "xmobar ~/.config/xmobar/xmobarrc" myPP

