module ArchOrange.KeyBindings (
  ArchOrange.KeyBindings.myKeyBindings
) where

    -- Base
import XMonad
import qualified XMonad.StackSet as W

    -- Actions
import XMonad.Actions.CopyWindow (kill1, copyToAll, killAllOtherCopies)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, doTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.FloatKeys

    -- Utilities
import XMonad.Util.NamedScratchpad (namedScratchpadAction)

    -- Data
import Data.Maybe (isJust)
import Data.Ratio

    -- Hooks
import XMonad.Hooks.ManageDocks (ToggleStruts(..))
import XMonad.Hooks.ManageHelpers

    -- Layouts
import XMonad.Layout.ResizableTile (MirrorResize(MirrorShrink, MirrorExpand))

    -- Layouts modifiers
import XMonad.Layout.LimitWindows (increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.Spacing
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

   -- Utilities
-- import XMonad.Util.Dmenu
import XMonad.Util.EZConfig (additionalKeysP)

   -- ArchOrange
import ArchOrange.Commands
import ArchOrange.Scratchpad (myScratchPads)
import ArchOrange.Menu as AOMenu (treeselectAction, myTreeConf, spawnSelected', myAppGrid, myBrowserGrid, myMessagingGrid, goToSelected, bringSelected, mygridConfig, myColorizer)

myCDmenu :: String
myCDmenu = "dmenu_run -c -bw 1 -l 20"

myTDmenu :: String
myTDmenu = "dmenu_run -x 10 -y 13 -z 1900 -p \"Run: \""

myHighPItems :: String
myHighPItems = " -hp gimp,kitty,lowriter,lodraw,localc,st,teams"

myKeyBindings :: XConfig l -> XConfig l
myKeyBindings conf = conf
  { modMask = mod4Mask -- Use the "Win" key for the mod key
  } 

  `additionalKeysP` -- Add some extra key bindings:

    -- KB_GROUP Xmonad
        [ ("M-C-r r", spawn "xmonad --recompile")       -- Recompiles xmonad
        , ("M-S-r r", spawn "xmonad --restart")         -- Restarts xmonad
        , ("M-S-r t", spawn "killall trayer && sleep 2 && trayer --edge top --align right --widthtype request --padding 8 --margin 40 --distance 12 --iconspacing 5 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 60 --tint 0x202020 --height 22")--" ++ colorTrayer ++ " --height 22)

    -- KB_GROUP Get Help
        , ("M-<F1>", spawn "bash ~/.config/shell/scripts/help/xmonad_keys.sh") -- Get list of keybindings
        , ("M-<F2>", spawn "bash ~/.config/shell/scripts/help/vim_keys.sh")
        , ("M-<F3>", spawn "bash ~/.config/shell/scripts/help/lf_keys.sh")
        , ("M-<F4>", spawn "bash ~/.config/shell/scripts/help/curriculum.sh")
        , ("M-<F5>", spawn "directscreencast")
        , ("M-<F11>", sendMessage ToggleStruts)
        , ("M-§", spawn "bash ~/.config/shell/scripts/help/typo_keys.sh")

    -- KB_GROUP Run Prompt
        , ("M-S-<Return>", spawn (myCDmenu ++ myHighPItems)) -- Dmenu
        , ("M-r",      spawn (myTDmenu ++ myHighPItems))

    -- KB_GROUP Other Dmenu Prompts
    -- In Xmonad and many tiling window managers, M-p is the default keybinding to
    -- launch dmenu_run, so I've decided to use M-p plus KEY for these dmenu scripts.
        , ("M-p h", spawn "dm-hub")           -- allows access to all dmscripts
        , ("M-p a", spawn "dm-sounds")        -- choose an ambient background
        , ("M-p c", spawn "dm-cal")           -- calendar
        , ("M-p k", spawn "dm-kill")          -- kill processes
        , ("M-p n", spawn "dm-note")          -- store one-line notes and copy them
        , ("M-p p", spawn "dm-pass")          -- passmenu
        , ("M-p q", spawn "dm-logout")        -- logout menu
        , ("M-p r", spawn "dm-radio")         -- online radio
        , ("M-p y", spawn "dm-youtube")       -- search youtube
        , ("M-p e", spawn "dm-emoji")         -- browse some emojis
        , ("M-p m", spawn "dm-mail")          -- copy email addresses
        , ("M-p t", spawn "dm-todo")          -- tasks
        , ("M-p i", spawn "dm-imdb")          -- imdb-ids
        -- , ("M-S-.", spawn "dm-wm-control")          -- tasks
        
        , ("M-<Insert>", spawn "dm-scan")
    -- KB_GROUP Useful programs to have a keybinding for launch
        , ("M-<Return>", spawn (myTerminal))

        -- Browsers
        , ("M-b b", spawn (myBrowser))
        , ("M-b o", spawn "brave")
        , ("M-b f", spawn "firefox")
        , ("M-b <Space>" , spawnSelected' myBrowserGrid)

        -- Chat
        , ("M-m m", spawn (myMail))
        , ("M-m p", spawn "qutebrowser --qt-arg name ProtonMail --target window https://mail.proton.me/u/0/inbox")
        , ("M-m t", spawn "teams")
        , ("M-m w", namedScratchpadAction myScratchPads "whatsapp")
        , ("M-m s", namedScratchpadAction myScratchPads "signal")
        , ("M-m <Space>" , spawnSelected' myMessagingGrid)

        -- file explorer
        , ("M-e e", spawn "kitty -e lfub")
        , ("M-e q", spawn myFiles)
        
        , ("M-v", spawn "keepassxc")
        
        , ("M-C-w", spawn "/home/azure/.config/shell/scripts/background/background.sh")
        , ("M-l", spawn "/home/azure/.config/shell/scripts/lock/lock -f -m")
        , ("M-S-l", spawn "dm-logout")

    -- KB_GROUP Scratchpads
    -- Toggle show/hide these programs.  They run on a hidden workspace.
    -- When you toggle them to show, it brings them to your current workspace.
    -- Toggle them to hide and it sends them back to hidden workspace (NSP).
        , ("M-s t", namedScratchpadAction myScratchPads "terminal")
        , ("M-s s", namedScratchpadAction myScratchPads "spotify")
        , ("M-s c", namedScratchpadAction myScratchPads "calculator")
        , ("M-s p", namedScratchpadAction myScratchPads "calendar")
        , ("M-s n", namedScratchpadAction myScratchPads "notes")
        , ("M-s e", namedScratchpadAction myScratchPads "slack")
        , ("M-s w", namedScratchpadAction myScratchPads "whatsapp")
        , ("M-s q", namedScratchpadAction myScratchPads "signal")

    -- KB_GROUP Kill windows
        , ("M-q" , killAllOtherCopies <+> myKill1)     -- Kill the currently focused client

    -- KB_GROUP Tree Select (CTR-t followed by a key)
        , ("M-t", AOMenu.treeselectAction AOMenu.myTreeConf)

    -- KB_GROUP Grid Select (CTR-g followed by a key)
        , ("M-g g", AOMenu.spawnSelected' AOMenu.myAppGrid)                 -- grid select favorite apps
        , ("M-g t", AOMenu.goToSelected $ AOMenu.mygridConfig AOMenu.myColorizer)  -- goto selected window
        , ("M-g b", AOMenu.bringSelected $ AOMenu.mygridConfig AOMenu.myColorizer) -- bring selected window


    -- KB_GROUP Workspaces
        , ("M-.", nextScreen)  -- Switch focus to next monitor
        , ("M-,", prevScreen)  -- Switch focus to prev monitor
        , ("M-S-<Right>", myShiftTo Next nonNSP) -- Shifts focused window to next ws if there is a focused window
        , ("M-S-<Left>", myShiftTo Prev nonNSP) -- Shifts focused window to prev ws ''
        , ("M-<Left>", moveTo Prev nonNSP)
        , ("M-<Right>", moveTo Next nonNSP)

    -- KB_GROUP Floating windows
        , ("M-C-f", sendMessage (T.Toggle "floats")) -- Toggles my 'floats' layout
        , ("M-C-t", withFocused $ windows . W.sink)  -- Push floating window back to tile
        , ("M-S-t", sinkAll)                       -- Push ALL floating windows to tile
        , ("M-S-s s", withFocused (keysMoveWindowTo (1910, 1070) (1, 1)) <+> withFocused (keysResizeWindow (-350, -350) (1, 1)) <+> windows copyToAll) -- make window sticky
        , ("M-S-s q", killAllOtherCopies <+> withFocused (keysResizeWindow (624, 624) (1%2, 1%2)) <+> withFocused (keysMoveWindowTo (960, 540) (1%2, 1%2))) -- kill all copies
        , ("M-C-s q", killAllOtherCopies)

    -- KB_GROUP Windows navigation
        , ("M-j", windows W.focusDown)    -- Move focus to the next window
        , ("M-<Down>", windows W.focusDown)    -- Move focus to the next window
        , ("M-k", windows W.focusUp)      -- Move focus to the prev window
        , ("M-<Up>", windows W.focusUp)      -- Move focus to the prev window
        , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window
        , ("M-S-j", windows W.swapDown)   -- Swap focused window with next window
        , ("M-S-k", windows W.swapUp)     -- Swap focused window with prev window
        , ("M-<Backspace>", promote)      -- Moves focused window to master, others maintain order
        , ("M-S-<Tab>", rotSlavesDown)    -- Rotate all windows except master and keep focus in place
        , ("M-C-<Tab>", rotAllDown)       -- Rotate all the windows in the current stack

    -- KB_GROUP Layouts
        , ("M-<Tab>", sendMessage NextLayout)           -- Switch to next layout
        , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full

    -- KB_GROUP Increase/decrease spacing (gaps)
        , ("C-M1-j", decWindowSpacing 4)         -- Decrease window spacing
        , ("C-M1-k", incWindowSpacing 4)         -- Increase window spacing
        , ("C-M1-h", decScreenSpacing 4)         -- Decrease screen spacing
        , ("C-M1-l", incScreenSpacing 4)         -- Increase screen spacing

    -- KB_GROUP Increase/decrease windows in the master pane or the stack
        , ("M-S-<Up>", sendMessage (IncMasterN 1))      -- Increase # of clients master pane
        , ("M-S-<Down>", sendMessage (IncMasterN (-1))) -- Decrease # of clients master pane
        , ("M-C-<Up>", increaseLimit)                   -- Increase # of windows
        , ("M-C-<Down>", decreaseLimit)                 -- Decrease # of windows

    -- KB_GROUP Window resizing
        , ("M-C-h", sendMessage Shrink)                   -- Shrink horiz window width
        , ("M-C-l", sendMessage Expand)                   -- Expand horiz window width
        , ("M-C-j", sendMessage MirrorShrink)          -- Shrink vert window width
        , ("M-C-k", sendMessage MirrorExpand)          -- Expand vert window width

    -- KB_GROUP Multimedia Keys
        , ("<XF86AudioPlay>", spawn "sp play")
        , ("<XF86AudioPrev>", spawn "sp prev")
        , ("<XF86AudioNext>", spawn "sp next")
        , ("<XF86AudioStop>", spawn "sp pause && sp prev")
        , ("<XF86AudioMute>", spawn "bash .config/shell/scripts/dunst/volume.sh mute")
        , ("<XF86AudioLowerVolume>", spawn "bash .config/shell/scripts/dunst/volume.sh down")
        , ("<XF86AudioRaiseVolume>", spawn "bash .config/shell/scripts/dunst/volume.sh up")
        , ("<XF86WLAN>", spawn "bash ~/.config/shell/scripts/networking/toggle-networking.sh")
        , ("<XF86HomePage>", spawn "qutebrowser")
        , ("M-<KP_Divide>", spawn "dm-websearch")
        , ("<XF86MonBrightnessUp>", spawn "bash .config/shell/scripts/dunst/brightness.sh up")
        , ("<XF86MonBrightnessDown>", spawn "bash .config/shell/scripts/dunst/brightness.sh down")
        , ("<Print>", spawn (myScreenshot))
        , ("M-S-<Print>", spawn (myRegionScreenshot))
        , ("M-C-<Print>", spawn "dm-shot")
        , ("M-M1-<Print>", spawn (myDelayedScreenshot))
        , ("M-<XF86XK_Launch1>", spawn (myRegionScreenshot))
        ]
-- The following lines are needed for named scratchpads.
          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))
{-
-- Non-numeric num pad keys, sorted by number 
numPadKeys = [ "M-<KP_End>",  "M-<KP_Down>",  "M-<KP_Page_Down>" -- 1, 2, 3
             , "M-<KP_Left>", "M-<KP_Begin>", "M-<KP_Right>"     -- 4, 5, 6
             , "M-<KP_Home>", "M-<KP_Up>",    "M-<KP_Page_Up>"   -- 7, 8, 9
             , "M-<KP_Insert>"] -- 0

myKeys :: [(String, X ())]
myKeys = myKeyBindings
    [((k), windows $ f i)
        | (i, k) <- zip myWorkspaces numPadKeys
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
    ]
-}
