module ArchOrange.Scratchpad (
  ArchOrange.Scratchpad.myScratchPads
) where

   -- Base
import XMonad
import qualified XMonad.StackSet as W

   -- Utilities
import XMonad.Util.NamedScratchpad

   -- ArchOrange
import ArchOrange.Commands

myScratchPads :: [NamedScratchpad]
myScratchPads = 
  [ NS "terminal" spawnTerm findTerm manageTerm
  , NS "calendar" spawnCalendar findCalendar manageCalendar
  , NS "protonmail" spawnProtonMail findProtonMail manageProtonMail
  -- , NS "mocp" spawnMocp findMocp manageMocp
  , NS "calculator" spawnCalc findCalc manageCalc
  , NS "notes" spawnNotes findNotes manageNotes
  , NS "spotify" spawnSpot findSpot manageSpot
  , NS "slack" spawnSlack findSlack manageSlack
  , NS "whatsapp" spawnWA findWA manageWA
  , NS "signal" spawnSig findSig manageSig
  ]
  where
    spawnTerm  = myTerminal ++ " -T scratchpad"
    findTerm   = title =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnCalendar  = myCalendar
    findCalendar   = className =? "Re.sonny.Tangram"
    manageCalendar = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.7
                 t = 0.95 -h
                 l = 0.85 -w
    spawnProtonMail  = "qutebrowser --qt-arg name ProtonMail --target window https://mail.proton.me/u/0/inbox"
    findProtonMail   = className =? "ProtonMail"
    manageProtonMail = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.7
                 t = 0.95 -h
                 l = 0.85 -w
    spawnNotes  = "joplin.AppImage"
    findNotes   = className =? "Joplin"
    manageNotes = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.7
                 t = 0.95 -h
                 l = 0.85 -w
    spawnSpot  = myMusic
    findSpot   = className =? "Spotify"
    manageSpot = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnMocp  = myTerminal ++ " -T mocp -e mocp"
    findMocp   = title =? "mocp"
    manageMocp = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnCalc  = "qalculate-gtk"
    findCalc   = className =? "Qalculate-gtk"
    manageCalc = customFloating $ W.RationalRect l t w h
               where
                 h = 0.5
                 w = 0.4
                 t = 0.75 -h
                 l = 0.70 -w
    spawnSlack  = "slack"
    findSlack   = className =? "Slack"
    manageSlack = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.7
                 t = 0.95 -h
                 l = 0.85 -w
    spawnWA  = "whatsapp-for-linux"
    findWA   = className =? "Whatsapp-for-linux"
    manageWA = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.7
                 t = 0.95 -h
                 l = 0.85 -w
    spawnSig  = "signal-desktop"
    findSig   = className =? "Signal"
    manageSig = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.7
                 t = 0.95 -h
                 l = 0.85 -w



