module ArchOrange.Layout (
  ArchOrange.Layout.myLayoutHook,
  ArchOrange.Layout.myWorkspaces,
  ArchOrange.Layout.myWorkspaceIndices,
  ArchOrange.Layout.clickable
) where

    -- Base
import XMonad

    -- Actions
import XMonad.Actions.MouseResize

    -- Data
import qualified Data.Map as M
import Data.Maybe (fromJust)

    -- Hooks
import XMonad.Hooks.ManageDocks (avoidStruts)

    -- Utils
import qualified XMonad.Util.Themes as Theme -- Tabbed

    -- Layouts
import XMonad.Layout.PerWorkspace
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.Decoration -- Tabbed

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

    -- Color scheme
import Colors.AzureOrange

import ArchOrange.Commands


myWorkspaces = [" cmd ", " www ", " sys ", " doc ", " gam ", " chat ", " med ", " gfx ", " vlt "]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True


-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 8
           $ spiral (6/7)
tabs     = renamed [Replace "tabs"]
           -- $ spacingRaw False (Border 10 0 10 10) True (Border 0 18 8 8) True -- with gaps
           $ spacingRaw False (Border 0 0 0 0) True (Border 8 8 8 8) True -- borderless
           $ tabbed shrinkText (Theme.theme smallDoomOne)--myTabTheme

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = color15
                 , inactiveColor       = color08
                 , activeBorderColor   = color15
                 , inactiveBorderColor = colorBack
                 , activeTextColor     = colorBack
                 , inactiveTextColor   = color16
                 -- , mySpacing           = 150
                 }

newTheme :: Theme.ThemeInfo
newTheme = Theme.TI "" "" "" def

smallDoomOne :: Theme.ThemeInfo
smallDoomOne =
    newTheme { Theme.themeName        = "smallDoomOne"
             , Theme.themeAuthor      = "AzureOrange"
             , Theme.themeDescription = "Small decorations"
             , Theme.theme            = def { fontName            = "xft:Ubuntu:bold:size=9:antialias=true:hinting=true"
                                            , activeColor         = color12
                                            , inactiveColor       = color08
                                            , urgentColor         = color02
                                            , activeBorderColor   = color12
                                            , inactiveBorderColor = color08
                                            , urgentBorderColor   = color02
                                            , activeTextColor     = colorBack
                                            , inactiveTextColor   = color16
                                            , urgentTextColor     = colorBack
                                            , decoHeight          = 13
                                            }
             }

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Ubuntu:bold:size=60"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#ffffff"
    }

-- The layout hook
myLayoutHook = showWName' myShowWNameTheme $ avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     onWorkspaces [" www ", " chat "] myTABS myTALL
                                 ||| onWorkspaces [" www ", " chat "] grid myTABS
                                 ||| onWorkspaces [" www ", " chat "] spirals grid
                                 ||| onWorkspaces [" www ", " chat "] floats spirals
                                 ||| onWorkspaces [" www ", " chat "] myTALL floats
                               where 
                                 myTALL = withBorder myBorderWidth tall
                                 myTABS = noBorders tabs
