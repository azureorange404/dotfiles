module ArchOrange.Menu (
  ArchOrange.Menu.myTreeConf,
  ArchOrange.Menu.treeselectAction,
  ArchOrange.Menu.spawnSelected',
  ArchOrange.Menu.myAppGrid,
  ArchOrange.Menu.myBrowserGrid,
  ArchOrange.Menu.myMessagingGrid,
  ArchOrange.Menu.mygridConfig,
  ArchOrange.Menu.myColorizer,
  goToSelected,
  bringSelected
) where

  -- Base
import XMonad

    -- Actions
import XMonad.Actions.GridSelect
import qualified XMonad.Actions.TreeSelect as TS -- Tree Select Menu

    -- Data
import Data.Tree -- Tree Select Menu

    -- Hooks
import XMonad.Hooks.WorkspaceHistory -- Tree Select Menu

    -- ArchOrange
import ArchOrange.Commands

-- START TreeSelect
-- treeSelect menu layout
myTreeConf = TS.TSConfig { TS.ts_hidechildren = True
                           , TS.ts_background   = 0xcc282c34
                           , TS.ts_font         = "xft:Ubuntu:bold"
                           , TS.ts_node         = (0xffc678dd, 0xff202328)
                           , TS.ts_nodealt      = (0xffc678dd, 0xff202020) 
                           , TS.ts_highlight    = (0xff000000, 0xff46D9FF) -- black, cyan
                           , TS.ts_extra        = 0xff94a187
                           , TS.ts_node_width   = 200
                           , TS.ts_node_height  = 30
                           , TS.ts_originX      = 0
                           , TS.ts_originY      = 250
                           , TS.ts_indent       = 80
                           , TS.ts_navigate     = TS.defaultNavigation
                           }

treeselectAction :: TS.TSConfig (X ()) -> X ()
treeselectAction myTreeConf = TS.treeselectAction myTreeConf
   [ Node (TS.TSNode "kitty"    "displays a kitten"           (spawn "kitty")) []
-- , Node (TS.TSNode "NAME" "DESCRIPTION" (return ()))
--     [ Node (TS.TSNode "" "" (spawn "")) []
--     , Node (TS.TSNode "" "" (spawn "")) []
--     ] Node (TS.TSNode "" "" (spawn "")) []
   , Node (TS.TSNode "files" "maximum power file manager" (spawn myFiles)) []
   , Node (TS.TSNode "accessories" "small utils for even smaller tasks" (return ()))
       -- [ Node (TS.TSNode "joplin" "notes and to-dos" (namedScratchpadAction myScratchPads "notes")) []
       -- , Node (TS.TSNode "calendar" "" (namedScratchpadAction myScratchPads "calendar")) []
       -- , Node (TS.TSNode "calculator" "" (namedScratchpadAction myScratchPads "calculator")) []
       [ Node (TS.TSNode "neovim" "neovide" (spawn "neovide")) []
       ]
   , Node (TS.TSNode "internet" "your browser to choose" (return ()))
       [ Node (TS.TSNode "brave" "standard browser" (spawn "brave-browser")) []
       , Node (TS.TSNode "opera" "work browser" (spawn "opera")) []
       , Node (TS.TSNode "firefox" "media browser" (spawn "firefox")) []
       , Node (TS.TSNode "librewolf" "librewolf" (spawn "librewolf")) []
       , Node (TS.TSNode "badwolf" "badwolf" (spawn "badwolf")) []
       ]
   , Node (TS.TSNode "office" "writing documents and stuff" (return ()))
       [ Node (TS.TSNode "libreoffice" "for all your office needs" (spawn "flatpak run org.libreoffice.LibreOffice")) []
       , Node (TS.TSNode "vim" "the best text editor there is" (spawn (myTerminal ++ "-e vim"))) []
       , Node (TS.TSNode "neovim" "neovide" (spawn "neovide")) []
       , Node (TS.TSNode "onenote" "proprietary shit from Microsoft" (spawn "flatpak run re.sonny.Tangram")) []
       ]
   , Node (TS.TSNode "social" "chatting and more" (return ()))
       [ Node (TS.TSNode "mail" "the go to tool for writing messages" (spawn "thunderbird")) []
       , Node (TS.TSNode "teams" "proprietary microshit" (spawn "teams")) []
       , Node (TS.TSNode "whatsapp" "facebooks shitty messenger everybody uses" (spawn "flatpak run re.sonny.Tangram")) []
       ]
   , Node (TS.TSNode "graphics" "since 1822" (return ()))
       [ Node (TS.TSNode "gimp" "edit everything and more" (spawn "gimp")) []
       , Node (TS.TSNode "inkscape" "vector graphics and stuff" (spawn "inkscape")) []
       ]
   , Node (TS.TSNode "video" "it's bad for your health man ..." (return ()))
       [ Node (TS.TSNode "kdenlive" "video editing for free suckers" (spawn "kdenlive.AppImage")) []
       ]
   , Node (TS.TSNode "system" "system" (return ()))
       [ Node (TS.TSNode "brightness" "set screen brightness" (return ()))
           [ Node (TS.TSNode "bright" "FULL POWER!!"                    (spawn "brightnessctl s 100%")) []
           , Node (TS.TSNode "normal" "Is it half bright or half dark?" (spawn "brightnessctl s 50%"))  []
           , Node (TS.TSNode "dim"    "Pretty damn dark."               (spawn "brightnessctl s 1%"))  []
           ]
       , Node (TS.TSNode "power" "powermenu" (return ())) 
           [ Node (TS.TSNode "lock" "It's like gambling."         (spawn "~/.config/shell/scripts/lock/lock -p -c")) []
           , Node (TS.TSNode "suspend" "That's the way to go."    (spawn "~/.config/shell/scripts/lock/lock -f -s")) []
           , Node (TS.TSNode "logout" "Why would you?"            (spawn "pkill xmonad")) []
           , Node (TS.TSNode "reboot" "See you later, alligator." (spawn "systemctl reboot")) []
           , Node (TS.TSNode "shutdown" "Bye, bye!"               (spawn "systemctl poweroff")) []
           ]
       ]
  ]

-- END TreeSelect

-- START GridSelect
myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                  (0x28,0x2c,0x34) -- lowest inactive bg
                  (0x28,0x2c,0x34) -- highest inactive bg
                  (0xc7,0x92,0xea) -- active bg
                  (0xc0,0xa7,0x9a) -- inactive fg
                  (0x28,0x2c,0x34) -- active fg

-- gridSelect menu layout
mygridConfig :: p -> GSConfig Window
-- mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
mygridConfig colorizer = (buildDefaultGSConfig greenColorizer)
    { gs_cellheight   = 40
    , gs_cellwidth    = 200
    , gs_cellpadding  = 6
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
    , gs_font         = myFont
    }

-- | A green monochrome colorizer based on window class
greenColorizer = colorRangeFromClassName
                     black            -- lowest inactive bg
                     (0x70,0xFF,0x70) -- highest inactive bg
                     black            -- active bg
                     white            -- inactive fg
                     white            -- active fg
  where black = minBound
        white = maxBound

-- spawnSelectedSP :: [(String, String)] -> X ()
-- spawnSelectedSP lst = gridselect conf lst >>= flip whenJust (namedScratchpadAction myScratchPads) --spawn
--     where conf = def
--                    { gs_cellheight   = 40
--                    , gs_cellwidth    = 200
--                    , gs_cellpadding  = 6
--                    , gs_originFractX = 0.5
--                    , gs_originFractY = 0.5
--                    , gs_font         = myFont
--                    }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def
                   { gs_cellheight   = 40
                   , gs_cellwidth    = 200
                   , gs_cellpadding  = 6
                   , gs_originFractX = 0.5
                   , gs_originFractY = 0.5
                   , gs_font         = myFont
                   }

myAppGrid = 
  [ ("Terminal", "kitty")
  , ("Gimp", "gimp")
  , ("Writer", "libreoffice --writer")
  , ("Calc", "libreoffice --calc")
  , ("Impress", "libreoffice --impress")
  , ("Audacity", "audacity")
  , ("Spotify", "spotify")
  -- , ("OneNote", "onenote")
  ] <+> myBrowserGrid <+> myMessagingGrid

myBrowserGrid =
  [ ("qutebrowser", "qutebrowser")
  -- , ("badwolf", "badwolf")
  , ("firefox", "firefox")
  , ("opera", "opera")
  , ("brave", "brave-browser")
  , ("librewolf", "librewolf")
  ]

myMessagingGrid = 
  [ ("mail", (myMail))
  , ("whatsapp", "whatsapp-for-linux")
  , ("signal", "signal-desktop")
  , ("teams", "teams")
  , ("proton", "qutebrowser --qt-arg name ProtonMail --target window https://mail.proton.me/u/0/inbox")
  ]

{-
myScratchpadGrid = [ ("terminal", (namedScratchpadAction myScratchPads "terminal"))
                        , ("spotify", (namedScratchpadAction myScratchPads "spotify"))
                        , ("calculator", (namedScratchpadAction myScratchPads "calculator"))
                        , ("calendar", (namedScratchpadAction myScratchPads "calendar"))
                        , ("notes", (namedScratchpadAction myScratchPads "notes"))
                        , ("whatsapp", (namedScratchpadAction myScratchPads "whatsapp"))
                        , ("signal", (namedScratchpadAction myScratchPads "signal"))
                        , ("slack", (namedScratchpadAction myScratchPads "slack"))
                        ]
-}

-- END GridSelect
